**Requirements**

*  NodeJs
*  Angular 8

**How to install**

*  git clone https://gitlab.com/rahilpeerbits/demo-frontend.git
*  cd demo-frontend
*  npm install
*  ng serve

You will be able to check application on http://localhost:4200