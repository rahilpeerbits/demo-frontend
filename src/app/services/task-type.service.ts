import { Injectable } from '@angular/core';
import { Observable, Subject, from, throwError, of } from 'rxjs';
import { User } from '../Models/user.model';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { MessageService } from './message.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import {Router} from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class TaskTypeService {

  constructor(public apiService: ApiService,  public msgService: MessageService) { }

  public getTasks(): Observable<any>{
    return this.apiService.get('process-type/list').pipe(map(res => {
      return res.data;
    }));
  }

  public createTask(event: any): Observable<any> {
    return this.apiService.post('process-type/create', event).pipe(map((res) => {
      if (res.status == 200) {
        this.msgService.success('Process Type Created');
        return res.data;
      }
      else {
        this.msgService.error('Unable to create');
        throwError('invalid');
      }
    }));
  }

  public editTask(event: any): Observable<any> {
    return this.apiService.post('process-type/edit', event).pipe(map((res) => {
      if (res.status == 200) {
        this.msgService.success('Process Type Edited');
        return res.data;
      }
      else {
        this.msgService.error('Unable to edit');
        throwError('invalid');
      }
    }));
  }

  public deleteTask(event: any): Observable<any> {
    return this.apiService.delete('process-type/delete', event.id).pipe(map((res) => {
      if (res.status == 200) {
        this.msgService.success('Process Type Deleted');
        return res.data;
      }
      else {
        this.msgService.error('Unable to delete');
        throwError('invalid');
      }
    }));
  }
}
