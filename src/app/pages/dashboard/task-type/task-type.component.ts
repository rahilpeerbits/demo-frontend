import { Component, OnInit } from '@angular/core';
import { TaskTypeService } from '../../../services/task-type.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-task-type',
  templateUrl: './task-type.component.html',
  styleUrls: ['./task-type.component.scss']
})
export class TaskTypeComponent implements OnInit {

  settings = {
    delete: {
      confirmDelete: true,

      deleteButtonContent: '<i class="nb-trash"></i>',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave:true
    },
    columns: {
      type: {
        title: 'Type',
        editor: {
          type: 'list',
          config: {
            selectText: 'Select',
            list: [
              {value: 'Process', title:'Process'},
              {value: 'Resource', title:'Resource'},
              {value: 'Task', title:'Task'}
            ],
          },
        },
        filter: {
          type: 'list',
          config: {
            selectText: 'Select',
            list: [
              {value: 'Process', title:'Process'},
              {value: 'Resource', title:'Resource'},
              {value: 'Task', title:'Task'}
            ],
          },
        },
      },
      code: {
        title: 'Code',
      },
      description: {
        title: 'Description',
      },
    },
  };

  source = new LocalDataSource();

  constructor(public taskTypeService: TaskTypeService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.taskTypeService.getTasks().subscribe(res => {
      this.source.load(res);
    });
  }

  onDeleteConfirm(event) {
    this.taskTypeService.deleteTask(event.data).subscribe((res) => {
      if(res){
        event.confirm.resolve(event.newData);
        this.getData();
      }
    });
  }

  onCreateConfirm(event) {
    this.taskTypeService.createTask(event.newData).subscribe((res) => {
      if(res){
        event.confirm.resolve(event.newData);
        this.getData();
      }
    });
  }

  onSaveConfirm(event) {
    this.taskTypeService.editTask(event.newData).subscribe((res) => {
      if(res){
        event.confirm.resolve(event.newData);
        this.getData();
      }
    });
  }

}
